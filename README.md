# Date: 23-03-2022

Hello Reader,

The complete demo is divided into 5 parts as follows :
1. Titles/introduction
2. "Where to check comparison parameters?" is to inform the viewer about the comparison / benchmarking scene
3. Actual comparison / benchmarking scene
4. OpenGL effects (like starfield and lighting, etc.) along with ocean FFT
5. End credits

In comparison / benchmarking scene currently showing few things like hardware name, type of hardware, frames per second rate, current polygon mode, number of vertices and triangles & mesh size
