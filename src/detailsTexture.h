#pragma once

#include "./textureLoading.h"

GLfloat alphaValue = 0.0f;

void initializeDetailsTexture(void)
{
    fprintf(gpFile, "\n+[%s @%d] begin::initializeDetailsTexture()]\n", __FILE__, __LINE__);

    // Code
    // Get Uniform Location
    textureLoadingDetails.mvpMatrixUniform = glGetUniformLocation(textureLoadingWatermark.shaderProgramObjectTexture, "u_mvpMatrix");
    textureLoadingDetails.textureSamplerUniform = glGetUniformLocation(textureLoadingWatermark.shaderProgramObjectTexture, "u_Texture_Sampler");
    textureLoadingDetails.alphaValueUniform = glGetUniformLocation(textureLoadingWatermark.shaderProgramObjectTexture, "u_alpha_value");

    const GLfloat quadTexCoords[] = {1.0f, 1.0f,
                                     0.0f, 1.0f,
                                     0.0f, 0.0f,
                                     1.0f, 0.0f};

    glGenVertexArrays(1, &textureLoadingDetails.vao_quad);
    glBindVertexArray(textureLoadingDetails.vao_quad);
    glGenBuffers(1, &textureLoadingDetails.vbo_position_quad);
    glBindBuffer(GL_ARRAY_BUFFER, textureLoadingDetails.vbo_position_quad);
    glBufferData(GL_ARRAY_BUFFER, (4 * 3 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(OUP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(OUP_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &textureLoadingDetails.vbo_texture_quad);
    glBindBuffer(GL_ARRAY_BUFFER, textureLoadingDetails.vbo_texture_quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_STATIC_DRAW);
    glVertexAttribPointer(OUP_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(OUP_ATTRIBUTE_TEXCOORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    if (LoadGLTexture(&textureLoadingDetails.gluiTextureImage, MAKEINTRESOURCE(DETAILS_TEXTURE)))
    {
        fprintf(gpFile, "\tinfo>> LoadGLTexture(DETAILS_TEXTURE) successful...\n");
    }

    fprintf(gpFile, "+[%s @%d] end::initializeDetailsTexture()]\n", __FILE__, __LINE__);
}

void displayDetailsTexture(void)
{
    // Code
    glEnable(GL_TEXTURE_2D);
    glUseProgram(textureLoadingWatermark.shaderProgramObjectTexture);

    // Textured quad
    mat4 modelViewMatrixDetails = mat4::identity();
    modelViewMatrixDetails = modelViewMatrixDetails * vmath::translate(0.0f, -1.009999f, -2.39f);
    modelViewMatrixDetails = modelViewMatrixDetails * vmath::rotate(-90.0f, 0.0f, 0.0f, 1.0f);

    glUniformMatrix4fv(textureLoadingDetails.mvpMatrixUniform, 1, GL_FALSE, (perspectiveProjectionMatrix * modelViewMatrixDetails));
    glUniform1f(textureLoadingDetails.alphaValueUniform, alphaValue);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textureLoadingDetails.gluiTextureImage);
    glUniform1i(textureLoadingDetails.textureSamplerUniform, 1);

    glBindVertexArray(textureLoadingDetails.vao_quad);

    GLfloat quadVerticesDetails[12];
    quadVerticesDetails[0] = -2.0f;
    quadVerticesDetails[1] = 2.15f;
    quadVerticesDetails[2] = 0.0f;

    quadVerticesDetails[3] = -2.0f;
    quadVerticesDetails[4] = -2.15f;
    quadVerticesDetails[5] = 0.0f;

    quadVerticesDetails[6] = 0.0f;
    quadVerticesDetails[7] = -2.15f;
    quadVerticesDetails[8] = 0.0f;

    quadVerticesDetails[9] = 0.0f;
    quadVerticesDetails[10] = 2.15f;
    quadVerticesDetails[11] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, textureLoadingDetails.vbo_position_quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVerticesDetails), quadVerticesDetails, GL_DYNAMIC_DRAW);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glUseProgram(0);
    glDisable(GL_TEXTURE_2D);
}

void updateDetailsTexture(void)
{
    // Code
}

void uninitializeDetailsTexture(void)
{
    fprintf(gpFile, "\n-[%s @%d] begin::uninitializeDetailsTexture()]\n", __FILE__, __LINE__);

    // Local Variable Declaration
    GLsizei ShaderCount;
    GLsizei index;
    GLuint *pShaders = NULL;

    // Code
    if (textureLoadingDetails.vao_quad)
    {
        glDeleteBuffers(1, &textureLoadingDetails.vao_quad);
        textureLoadingDetails.vao_quad = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingDetails.vao_quad deleted successfully...\n");
    }

    if (textureLoadingDetails.vbo_position_quad)
    {
        glDeleteBuffers(1, &textureLoadingDetails.vbo_position_quad);
        textureLoadingDetails.vbo_position_quad = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingDetails.vbo_position_quad deleted successfully...\n");
    }

    if (textureLoadingDetails.vbo_texture_quad)
    {
        glDeleteBuffers(1, &textureLoadingDetails.vbo_texture_quad);
        textureLoadingDetails.vbo_texture_quad = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingDetails.vbo_texture_quad deleted successfully...\n");
    }

    if (textureLoadingDetails.gluiTextureImage)
    {
        glDeleteTextures(1, &textureLoadingDetails.gluiTextureImage);
        textureLoadingDetails.gluiTextureImage = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingDetails.gluiTextureImage deleted successfully...\n");
    }

    if (textureLoadingWatermark.shaderProgramObjectTexture)
    {
        // Safe Shader Cleaup
        glUseProgram(textureLoadingWatermark.shaderProgramObjectTexture);

        glGetProgramiv(textureLoadingWatermark.shaderProgramObjectTexture, GL_ATTACHED_SHADERS, &ShaderCount);
        pShaders = (GLuint *)malloc(sizeof(GLuint) * ShaderCount);
        if (pShaders == NULL)
        {
            fprintf(gpFile, "\terror>> [%s@%d] malloc() failed inside uninitializeFontRendering()...\n", __FILE__, __LINE__);
        }

        glGetAttachedShaders(textureLoadingWatermark.shaderProgramObjectTexture, ShaderCount, &ShaderCount, pShaders);
        for (index = 0; index < ShaderCount; index++)
        {
            glDetachShader(textureLoadingWatermark.shaderProgramObjectTexture, pShaders[index]);
            glDeleteShader(pShaders[index]);
            pShaders[index] = 0;
        }

        free(pShaders);

        glDeleteShader(textureLoadingWatermark.shaderProgramObjectTexture);
        textureLoadingWatermark.shaderProgramObjectTexture = 0;

        glUseProgram(0);

        fprintf(gpFile, "\tinfo>> textureLoadingWatermark.shaderProgramObjectTexture safe released successfully...\n");
    }

    fprintf(gpFile, "-[%s @%d] end::uninitializeDetailsTexture()]\n", __FILE__, __LINE__);
}
