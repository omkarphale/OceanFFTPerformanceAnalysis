#pragma once

#include "../header/commongl.h"
#include "./textureLoading.h"

#define DELAY_CHANGE_VALUE 0.0015f

GLuint fadingVertexShaderObject;
GLuint fadingFragmentShaderObject;
GLuint fadingShaderProgramObject;

GLuint vao_quad;
GLuint vbo_position_quad;
GLuint vbo_color_quad;

GLuint alphaValueUniform;

void initializeSceneFadeIn(void)
{
    fprintf(gpFile, "\n+[%s @%d] begin::initializeSceneFadeIn()]\n", __FILE__, __LINE__);

    // Local Variable Declaration
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    GLint iShaderLinkerStatus = 0;
    GLchar *szInfoLogBuffer = NULL;

    // Code
    // Vertex Shader - Creating Shader
    fadingVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar *pglcVertexShaderSourceCode =
        "#version 430 core                                               \n"
        "                                                                \n"
        "in vec4 vPosition;                                              \n"
        "                                                                \n"
        "uniform float u_alpha_value;                                    \n"
        "                                                                \n"
        "out vec4 out_vColor;                                            \n"
        "                                                                \n"
        "void main(void)                                                 \n"
        "{                                                               \n"
        "   vec4 vColor;                                                 \n"
        "   out_vColor[0] = 0.0;                                         \n"
        "   out_vColor[1] = 0.0;                                         \n"
        "   out_vColor[2] = 0.0;                                         \n"
        "   out_vColor[3] = u_alpha_value;                               \n"
        "                                                                \n"
        "	gl_Position = vPosition;                                     \n"
        "}                                                               \n";

    glShaderSource(fadingVertexShaderObject, 1, (const GLchar **)&pglcVertexShaderSourceCode, NULL);

    // Compiling Shader
    glCompileShader(fadingVertexShaderObject);

    // Error Checking
    glGetShaderiv(fadingVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fadingVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLogBuffer = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
            if (szInfoLogBuffer != NULL)
            {
                GLsizei written;

                glGetShaderInfoLog(fadingVertexShaderObject, iInfoLogLength, &written, szInfoLogBuffer);

                fprintf(gpFile, "\n[Vertex Shader Compilation Error Log : %s]\n", szInfoLogBuffer);
                free(szInfoLogBuffer);
                DestroyWindow(ghwnd);
            }
        }
    }

    // Fragment Shader - Creating Shader
    fadingFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *pglcFragmentShaderSourceCode =
        "#version 430 core                                              \n"
        "                                                               \n"
        "in vec4 out_vColor;                                            \n"
        "                                                               \n"
        "out vec4 FragmentColor;                                        \n"
        "                                                               \n"
        "void main(void)                                                \n"
        "{                                                              \n"
        "	FragmentColor = out_vColor;                                 \n"
        "}                                                              \n";

    glShaderSource(fadingFragmentShaderObject, 1, (const GLchar **)&pglcFragmentShaderSourceCode, NULL);

    // Compiling Shader
    glCompileShader(fadingFragmentShaderObject);

    // Error Checking
    glGetShaderiv(fadingFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fadingFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLogBuffer = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
            if (szInfoLogBuffer != NULL)
            {
                GLsizei written;

                glGetShaderInfoLog(fadingFragmentShaderObject, iInfoLogLength, &written, szInfoLogBuffer);

                fprintf(gpFile, "\n[Fragment Shader Compilation Error Log : %s]\n", szInfoLogBuffer);
                free(szInfoLogBuffer);
                DestroyWindow(ghwnd);
            }
        }
    }

    // Shader Program - Create Shader Program
    fadingShaderProgramObject = glCreateProgram();

    glAttachShader(fadingShaderProgramObject, fadingVertexShaderObject);   // Attach Vertex Shader To Shader Program
    glAttachShader(fadingShaderProgramObject, fadingFragmentShaderObject); // Attach Fragment Shader To Shader Program

    // Bind Vertex Shader Position Attribute
    glBindAttribLocation(fadingShaderProgramObject, OUP_ATTRIBUTE_POSITION, "vPosition");

    // Link Shader Program
    glLinkProgram(fadingShaderProgramObject);

    // Error Checking
    glGetProgramiv(fadingShaderProgramObject, GL_LINK_STATUS, &iShaderLinkerStatus);
    if (iShaderLinkerStatus == GL_FALSE)
    {
        glGetShaderiv(fadingShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLogBuffer = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
            if (szInfoLogBuffer != NULL)
            {
                GLsizei written;

                glGetShaderInfoLog(fadingShaderProgramObject, iInfoLogLength, &written, szInfoLogBuffer);

                fprintf(gpFile, "\n[Shader Program Linking Error Log : %s]\n", szInfoLogBuffer);
                free(szInfoLogBuffer);
                DestroyWindow(ghwnd);
            }
        }
    }

    // Get Uniform Location
    alphaValueUniform = glGetUniformLocation(fadingShaderProgramObject, "u_alpha_value");

    const GLfloat quadVertices[] = {
        +2.0f, +1.0f, 0.0f, //
        -2.0f, +1.0f, 0.0f, //
        -2.0f, -1.0f, 0.0f, //
        +2.0f, -1.0f, 0.0f, //
    };

    glGenVertexArrays(1, &vao_quad);
    glBindVertexArray(vao_quad);
    // vbo_position_rectangle
    glGenBuffers(1, &vbo_position_quad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(OUP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(OUP_ATTRIBUTE_POSITION);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    fprintf(gpFile, "+[%s @%d] end::initializeSceneFadeIn()]\n", __FILE__, __LINE__);
}

void displaySceneFadeingQuad(GLfloat delayResetLocal)
{
    // Code
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glUseProgram(fadingShaderProgramObject);

    glUniform1f(alphaValueUniform, delayResetLocal);

    glBindVertexArray(vao_quad);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    glUseProgram(0);

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
}

void uninitializeSceneFadeIn(void)
{
    fprintf(gpFile, "\n-[%s @%d] begin::uninitializeSceneFadeIn()]\n", __FILE__, __LINE__);

    // Local Variable Declaration
    GLsizei ShaderCount;
    GLsizei index;
    GLuint *pShaders = NULL;

    // Code
    if (vao_quad)
    {
        glDeleteBuffers(1, &vao_quad);
        vao_quad = 0;

        fprintf(gpFile, "\tinfo>> vao_quad deleted successfully...\n");
    }

    if (vbo_position_quad)
    {
        glDeleteBuffers(1, &vbo_position_quad);
        vbo_position_quad = 0;

        fprintf(gpFile, "\tinfo>> vbo_position_quad deleted successfully...\n");
    }

    if (fadingShaderProgramObject)
    {
        // Safe Shader Cleaup
        glUseProgram(fadingShaderProgramObject);

        glGetProgramiv(fadingShaderProgramObject, GL_ATTACHED_SHADERS, &ShaderCount);
        pShaders = (GLuint *)malloc(sizeof(GLuint) * ShaderCount);
        if (pShaders == NULL)
        {
            fprintf(gpFile, "\terror>> [%s@%d] malloc() failed inside uninitializeFontRendering()...\n", __FILE__, __LINE__);
        }

        glGetAttachedShaders(fadingShaderProgramObject, ShaderCount, &ShaderCount, pShaders);
        for (index = 0; index < ShaderCount; index++)
        {
            glDetachShader(fadingShaderProgramObject, pShaders[index]);
            glDeleteShader(pShaders[index]);
            pShaders[index] = 0;
        }

        free(pShaders);

        glDeleteShader(fadingShaderProgramObject);
        fadingShaderProgramObject = 0;

        glUseProgram(0);

        fprintf(gpFile, "\tinfo>> fadingShaderProgramObject safe released successfully...\n");
    }

    fprintf(gpFile, "-[%s @%d] end::uninitializeSceneFadeIn()]\n", __FILE__, __LINE__);
}
