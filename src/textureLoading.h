#pragma once

#include "../header/commongl.h"
#include "./moonSphere.h"

// LoadGLTexture() Definition
bool LoadGLTexture(GLuint *gluiTexture, TCHAR szResourceID[])
{
    // Local Variable Declaration
    bool bResult = false;
    HBITMAP hBitmap = NULL;
    BITMAP bmp;

    // Code
    hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), szResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
    if (hBitmap)
    {
        bResult = true;
        GetObject(hBitmap, sizeof(BITMAP), &bmp);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glGenTextures(1, gluiTexture);
        glBindTexture(GL_TEXTURE_2D, *gluiTexture);

        // Setting Texture Parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        // Actually Pushing The Data To Memory With The Help Of Graphic Driver
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
        glGenerateMipmap(GL_TEXTURE_2D); // gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

        DeleteObject(hBitmap);
    }

    return (bResult);
}

void initializeAstronautPicture(void)
{
    fprintf(gpFile, "\n+[%s @%d] begin::initializeAstronautPicture()]\n", __FILE__, __LINE__);

    // Local Function Declaration
    bool LoadGLTexture(GLuint * gluiTexture, TCHAR szResourceID[]);

    // Local Variable Declaration
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    GLint iShaderLinkerStatus = 0;
    GLchar *szInfoLogBuffer = NULL;

    // Code
    // Vertex Shader - Creating Shader
    textureLoadingWatermark.vertexShaderObjectTexture = glCreateShader(GL_VERTEX_SHADER);
    const GLchar *pglcVertexShaderSourceCode =  
        "#version 430 core                                                                                                              \n"
        "                                                                                                                               \n"
        "in vec4 vPosition;                                                                                                             \n"
        "in vec2 vTexCoord;                                                                                                             \n"
        "                                                                                                                               \n"
        "uniform mat4 u_mvpMatrix;                                                                                                      \n"
        "                                                                                                                               \n"
        "out vec2 out_vTexCoord;                                                                                                        \n"
        "                                                                                                                               \n"
        "void main(void)                                                                                                                \n"
        "{                                                                                                                              \n"
        "   gl_Position = u_mvpMatrix * vPosition;                                                                                      \n"
        "   out_vTexCoord = vTexCoord;                                                                                                  \n"
        "}                                                                                                                              \n";

    glShaderSource(textureLoadingWatermark.vertexShaderObjectTexture, 1, (const GLchar **)&pglcVertexShaderSourceCode, NULL);

    // Compiling Shader
    glCompileShader(textureLoadingWatermark.vertexShaderObjectTexture);

    // Error Checking
    glGetShaderiv(textureLoadingWatermark.vertexShaderObjectTexture, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(textureLoadingWatermark.vertexShaderObjectTexture, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLogBuffer = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
            if (szInfoLogBuffer != NULL)
            {
                GLsizei written;

                glGetShaderInfoLog(textureLoadingWatermark.vertexShaderObjectTexture, iInfoLogLength, &written, szInfoLogBuffer);

                fprintf(gpFile, "\n\terror>> [Vertex Shader Compilation Error Log : %s]\n", szInfoLogBuffer);
                free(szInfoLogBuffer);
                DestroyWindow(ghwnd);
            }
        }
    }

    // Fragment Shader - Creating Shader
    textureLoadingWatermark.fragmentShaderObjectTexture = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *pglcFragmentShaderSourceCode =
        "                                                                                                                               \n"
        "#version 430 core                                                                                                              \n"
        "in vec2 out_vTexCoord;                                                                                                         \n"
        "                                                                                                                               \n"
        "uniform sampler2D u_Texture_Sampler;                                                                                           \n"
        "uniform float u_alpha_value;                                                                                                   \n"
        "                                                                                                                               \n"
        "out vec4 FragmentColor;                                                                                                        \n"
        "                                                                                                                               \n"
        "void main(void)                                                                                                                \n"
        "{                                                                                                                              \n"
        "   FragmentColor.rgb = texture(u_Texture_Sampler, out_vTexCoord).rgb;                                                          \n"
        "   FragmentColor.a = u_alpha_value;                                                                                            \n"
        "                                                                                                                               \n"
        "   if((FragmentColor.r < 0.15) && (FragmentColor.g < 0.15) && (FragmentColor.r < 0.15))                                        \n"
        "   {                                                                                                                           \n"
        "       discard;                                                                                                                \n"
        "   }                                                                                                                           \n"
        "}                                                                                                                              \n";

    glShaderSource(textureLoadingWatermark.fragmentShaderObjectTexture, 1, (const GLchar **)&pglcFragmentShaderSourceCode, NULL);

    // Compiling Shader
    glCompileShader(textureLoadingWatermark.fragmentShaderObjectTexture);

    // Error Checking
    glGetShaderiv(textureLoadingWatermark.fragmentShaderObjectTexture, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(textureLoadingWatermark.fragmentShaderObjectTexture, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLogBuffer = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
            if (szInfoLogBuffer != NULL)
            {
                GLsizei written;

                glGetShaderInfoLog(textureLoadingWatermark.fragmentShaderObjectTexture, iInfoLogLength, &written, szInfoLogBuffer);

                fprintf(gpFile, "\n\terror>> [Fragment Shader Compilation Error Log : %s]\n", szInfoLogBuffer);
                free(szInfoLogBuffer);
                DestroyWindow(ghwnd);
            }
        }
    }

    // Shader Program
    // Create Shader Program
    textureLoadingWatermark.shaderProgramObjectTexture = glCreateProgram();

    glAttachShader(textureLoadingWatermark.shaderProgramObjectTexture, textureLoadingWatermark.vertexShaderObjectTexture);   // Attach Vertex Shader To Shader Program
    glAttachShader(textureLoadingWatermark.shaderProgramObjectTexture, textureLoadingWatermark.fragmentShaderObjectTexture); // Attach Fragment Shader To Shader Program

    // Bind Vertex Shader Position Attribute
    glBindAttribLocation(textureLoadingWatermark.shaderProgramObjectTexture, OUP_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(textureLoadingWatermark.shaderProgramObjectTexture, OUP_ATTRIBUTE_TEXCOORD, "vTexCoord");

    // Link Shader Program
    glLinkProgram(textureLoadingWatermark.shaderProgramObjectTexture);

    // Error Checking
    glGetProgramiv(textureLoadingWatermark.shaderProgramObjectTexture, GL_LINK_STATUS, &iShaderLinkerStatus);
    if (iShaderLinkerStatus == GL_FALSE)
    {
        glGetShaderiv(textureLoadingWatermark.shaderProgramObjectTexture, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLogBuffer = (GLchar *)malloc(iInfoLogLength * sizeof(GLchar));
            if (szInfoLogBuffer != NULL)
            {
                GLsizei written;

                glGetShaderInfoLog(textureLoadingWatermark.shaderProgramObjectTexture, iInfoLogLength, &written, szInfoLogBuffer);

                fprintf(gpFile, "\n\terror>> [Shader Program Linking Error Log : %s]\n", szInfoLogBuffer);
                free(szInfoLogBuffer);
                DestroyWindow(ghwnd);
            }
        }
    }

    // Get Uniform Location
    textureLoadingWatermark.mvpMatrixUniform = glGetUniformLocation(textureLoadingWatermark.shaderProgramObjectTexture, "u_mvpMatrix");
    textureLoadingWatermark.textureSamplerUniform = glGetUniformLocation(textureLoadingWatermark.shaderProgramObjectTexture, "u_Texture_Sampler");
    textureLoadingWatermark.alphaValueUniform = glGetUniformLocation(textureLoadingWatermark.shaderProgramObjectTexture, "u_alpha_value");
 
    // Code
    const GLfloat quadTexCoords[] = {1.0f, 1.0f,
                                     0.0f, 1.0f,
                                     0.0f, 0.0f,
                                     1.0f, 0.0f};

    glGenVertexArrays(1, &textureLoadingWatermark.vao_quad);
    glBindVertexArray(textureLoadingWatermark.vao_quad);
    glGenBuffers(1, &textureLoadingWatermark.vbo_position_quad);
    glBindBuffer(GL_ARRAY_BUFFER, textureLoadingWatermark.vbo_position_quad);
    glBufferData(GL_ARRAY_BUFFER, (4 * 3 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(OUP_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(OUP_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &textureLoadingWatermark.vbo_texture_quad);
    glBindBuffer(GL_ARRAY_BUFFER, textureLoadingWatermark.vbo_texture_quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadTexCoords), quadTexCoords, GL_STATIC_DRAW);
    glVertexAttribPointer(OUP_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(OUP_ATTRIBUTE_TEXCOORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    if (LoadGLTexture(&textureLoadingWatermark.gluiTextureImage, MAKEINTRESOURCE(ASTRONAUT_IMAGE_TEXTURE)))
    {
        fprintf(gpFile, "\tinfo>> LoadGLTexture(ASTRONAUT_IMAGE_TEXTURE) successful...\n");
    }

    fprintf(gpFile, "+[%s @%d] end::initializeAstronautPicture()]\n", __FILE__, __LINE__);
}

void displayAstronautPicture(void)
{
    // Code
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glUseProgram(textureLoadingWatermark.shaderProgramObjectTexture);

    // Textured quad
    mat4 modelViewMatrix = mat4::identity();
    modelViewMatrix = modelViewMatrix * vmath::translate(1.85f, -1.0f, -2.75f);

    glUniformMatrix4fv(textureLoadingWatermark.mvpMatrixUniform, 1, GL_FALSE, (perspectiveProjectionMatrix * modelViewMatrix));
    glUniform1f(textureLoadingWatermark.alphaValueUniform, 0.4f);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureLoadingWatermark.gluiTextureImage);
    glUniform1i(textureLoadingWatermark.textureSamplerUniform, 0);

    glBindVertexArray(textureLoadingWatermark.vao_quad);

    GLfloat quadVertices[12];

    quadVertices[0] = 0.1f;
    quadVertices[1] = 0.1f;
    quadVertices[2] = 0.0f;

    quadVertices[3] = -0.1f;
    quadVertices[4] = 0.1f;
    quadVertices[5] = 0.0f;

    quadVertices[6] = -0.1f;
    quadVertices[7] = -0.1f;
    quadVertices[8] = 0.0f;

    quadVertices[9] = 0.1f;
    quadVertices[10] = -0.1f;
    quadVertices[11] = 0.0f;

    glBindBuffer(GL_ARRAY_BUFFER, textureLoadingWatermark.vbo_position_quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_DYNAMIC_DRAW);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glUseProgram(0);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
}

void updateAstronautPicture(void)
{
    // Code
}

void uninitializeAstronautPicture(void)
{
    fprintf(gpFile, "\n-[%s @%d] begin::uninitializeAstronautPicture()]\n", __FILE__, __LINE__);

    // Local Variable Declaration
    GLsizei ShaderCount;
    GLsizei index;
    GLuint *pShaders = NULL;

    // Code
    if (textureLoadingWatermark.vao_quad)
    {
        glDeleteBuffers(1, &textureLoadingWatermark.vao_quad);
        textureLoadingWatermark.vao_quad = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingWatermark.vao_quad deleted successfully...\n");
    }

    if (textureLoadingWatermark.vbo_position_quad)
    {
        glDeleteBuffers(1, &textureLoadingWatermark.vbo_position_quad);
        textureLoadingWatermark.vbo_position_quad = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingWatermark.vbo_position_quad deleted successfully...\n");
    }

    if (textureLoadingWatermark.vbo_texture_quad)
    {
        glDeleteBuffers(1, &textureLoadingWatermark.vbo_texture_quad);
        textureLoadingWatermark.vbo_texture_quad = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingWatermark.vbo_texture_quad deleted successfully...\n");
    }

    if (textureLoadingWatermark.gluiTextureImage)
    {
        glDeleteTextures(1, &textureLoadingWatermark.gluiTextureImage);
        textureLoadingWatermark.gluiTextureImage = 0;

        fprintf(gpFile, "\tinfo>> textureLoadingWatermark.gluiTextureImage deleted successfully...\n");
    }

    if (textureLoadingWatermark.shaderProgramObjectTexture)
    {
        // Safe Shader Cleaup
        glUseProgram(textureLoadingWatermark.shaderProgramObjectTexture);

        glGetProgramiv(textureLoadingWatermark.shaderProgramObjectTexture, GL_ATTACHED_SHADERS, &ShaderCount);
        pShaders = (GLuint *)malloc(sizeof(GLuint) * ShaderCount);
        if (pShaders == NULL)
        {
            fprintf(gpFile, "\terror>> [%s@%d] malloc() failed inside uninitializeFontRendering()...\n", __FILE__, __LINE__);
        }

        glGetAttachedShaders(textureLoadingWatermark.shaderProgramObjectTexture, ShaderCount, &ShaderCount, pShaders);
        for (index = 0; index < ShaderCount; index++)
        {
            glDetachShader(textureLoadingWatermark.shaderProgramObjectTexture, pShaders[index]);
            glDeleteShader(pShaders[index]);
            pShaders[index] = 0;
        }

        free(pShaders);

        glDeleteShader(textureLoadingWatermark.shaderProgramObjectTexture);
        textureLoadingWatermark.shaderProgramObjectTexture = 0;

        glUseProgram(0);

        fprintf(gpFile, "\tinfo>> textureLoadingWatermark.shaderProgramObjectTexture safe released successfully...\n");
    }

    fprintf(gpFile, "-[%s @%d] end::uninitializeAstronautPicture()]\n", __FILE__, __LINE__);
}
