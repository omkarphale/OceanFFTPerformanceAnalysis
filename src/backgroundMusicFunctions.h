#pragma once

#include "../header/commongl.h"

// initializeOpenAL() Definition
void initializeOpenAL(const char *sourceFileName, unsigned int *buffer, unsigned int *sourceFile, ALvoid *alData)
{
    fprintf(gpFile, "\n+[%s @%d] begin::initializeOpenAL()]\n", __FILE__, __LINE__);

    // Local Variable Declaration
    int channel, sampleRate, beatsPerSecond, size;
    device = alcOpenDevice(NULL);

    // Code
    if (device)
    {
        fprintf(gpFile, "\tinfo>> device created successfully...\n");
        context = alcCreateContext(device, NULL);

        if (context != NULL)
        {
            fprintf(gpFile, "\tinfo>> context created successfully...\n");
        }

        alcMakeContextCurrent(context);

        alGenBuffers(1, buffer);
        alGenSources(1, sourceFile);

        if (sourceFile != NULL)
        {
            fprintf(gpFile, "\tinfo>> sourceFile generated successfully...\n");
        }

        alData = loadWav(sourceFileName, &channel, &sampleRate, &beatsPerSecond, &size);
        if (alData)
        {
            fprintf(gpFile, "\tinfo>> wave file loaded successfully...\n");
        }

        if (channel == 1)
        {
            if (beatsPerSecond == 8)
            {
                alFormat = AL_FORMAT_MONO8;
                fprintf(gpFile, "\tinfo>> mono8 selected...\n");
            }
            else
            {
                alFormat = AL_FORMAT_MONO16;
                fprintf(gpFile, "\tinfo>> mono16 selected...\n");
            }
        }
        else
        {
            if (beatsPerSecond == 8)
            {
                alFormat = AL_FORMAT_STEREO8;
                fprintf(gpFile, "\tinfo>> stereo8 selected...\n");
            }
            else
            {
                alFormat = AL_FORMAT_STEREO16;
                fprintf(gpFile, "\tinfo>> stereo16 selected...\n");
            }
        }

        alBufferData(*buffer, alFormat, alData, size, sampleRate);

        alSourcei(*sourceFile, AL_BUFFER, *buffer);
    }

    fprintf(gpFile, "+[%s @%d] end::initializeOpenAL()]\n", __FILE__, __LINE__);
}

// uninitializeOpenAL() Definition
void uninitializeOpenAL(void)
{
    fprintf(gpFile, "\n-[%s @%d] begin::uninitializeOpenAL()]\n", __FILE__, __LINE__);

    // Code
    if (sourceAlways)
    {
        alDeleteSources(1, &sourceAlways);
        sourceAlways = 0;

        fprintf(gpFile, "\tinfo>> \'sourceAlways\' deleted successfully...\n");
    }

    if (bufferAlways)
    {
        alDeleteBuffers(1, &bufferAlways);
        bufferAlways = 0;

        fprintf(gpFile, "\tinfo>> \'bufferAlways\' deleted successfully...\n");
    }
    
    fprintf(gpFile, "-[%s @%d] end::uninitializeOpenAL()]\n", __FILE__, __LINE__);
}
