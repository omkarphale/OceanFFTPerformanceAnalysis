cls

del "./main.exe" "./CodeExecutionLog.txt"

nvcc.exe -c -o oceanFFT_kernel.cu.obj "src/oceanFFT_kernel.cu"

cl.exe "src/main.cpp" /c /EHsc /I "C:\glew-2.1.0\include" /I "C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.1\include" /I "C:\Program Files (x86)\OpenAL 1.1 SDK\include" /I "C:\freetype-2.11.0\include" /I "C:\Program Files (x86)\OpenAL 1.1 SDK\include"
rc.exe "res/seminarProject.rc"
link.exe "./main.obj" "./oceanFFT_kernel.cu.obj" "res/SeminarProject.res" /LIBPATH:"C:\glew-2.1.0\lib\Release\x64" /LIBPATH:"C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v11.1\lib\x64" /LIBPATH:"C:\Program Files (x86)\OpenAL 1.1 SDK\libs\Win64" /LIBPATH:"C:\freetype-2.11.0\output\Release" gdi32.lib user32.lib /SUBSYSTEM:WINDOWS /MACHINE:x64
main.exe

del "./main.obj" "./oceanFFT_kernel.cu.obj" "res/SeminarProject.res"
